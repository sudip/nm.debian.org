#!/usr/bin/python3

import argparse
import sys
import logging
import json
import os
import datetime
import gzip
from contextlib import contextmanager

log = logging.getLogger("summarize")


class Fail(Exception):
    pass


class Summary:
    """
    Maintain a summary of all changes to the nm.debian.org database
    """
    def __init__(self):
        # List of (timestamp, person) indexed by lookup key
        self.people = {}

    @contextmanager
    def open(self, fn, mode):
        if fn.endswith(".gz"):
            with gzip.open(fn, mode) as fd:
                yield fd
        else:
            with open(fn, mode) as fd:
                yield fd

    def load(self, fn):
        try:
            with self.open(fn, "rt") as fd:
                self.people = json.load(fd)
        except FileNotFoundError:
            pass

    def save(self, fn):
        with self.open(fn, "wt") as fd:
            json.dump(self.people, fd)

    def add_dir(self, dirname):
        found = []
        for fn in os.listdir(dirname):
            if len(fn) == 21:
                dt = datetime.datetime.strptime(fn, "%Y%m%d-db-full.json")
            elif len(fn) == 27:
                dt = datetime.datetime.strptime(fn, "%Y%m%d%H%M%S-db-full.json")
            elif len(fn) == 28:
                dt = datetime.datetime.strptime(fn, "%Y%m%d-%H%M%S-db-full.json")
            else:
                log.error("%s: unrecognised file name", fn)
                continue
            log.debug("Found %s", dt)

            dt = dt.strftime("%Y%m%d%H%M%S")

            found.append((dt, fn))

        found.sort()

        for dt, fn in found:
            with open(os.path.join(dirname, fn), "rt") as fd:
                self.add_dump(fn, dt, json.load(fd))

    def add_dump(self, fn, dt, data):
        if isinstance(data, list):
            self.add_dump_v0(fn, dt, data)
        else:
            version = data["version"]
            if version == 1:
                self.add_dump_v1(fn, dt, data)
            else:
                raise NotImplementedError("{}: cannot load dump version {}".format(fn, version))

    def add_dump_v0(self, fn, dt, data):
        new = 0
        updated = 0
        unchanged = 0
        keys_found = set()

        for person in data:
            key = person["key"]
            keys_found.add(key)
            history = self.people.get(key)
            if history is None:
                new += 1
                self.people[key] = [(dt, person)]
            elif history[-1][0] > dt:
                log.warn("%s: last seen for %s is from %s but found earlier %s: skipped", fn, key, history[-1][0], dt)
            elif history[-1][1] != person:
                updated += 1
                history.append((dt, person))
            else:
                unchanged += 1

        removed = 0
        for key in self.people.keys() - keys_found:
            history = self.people.get(key)
            if history[-1][1] is not None:
                history.append((dt, None))
                removed += 1

        log.info("%s: %d new, %d updated, %d unchanged %d removed", fn, new, updated, unchanged, removed)

    def add_dump_v1(self, fn, dt, data):
        new = 0
        updated = 0
        unchanged = 0
        keys_found = set()

        for person in data["people"]:
            key = person["lookup_key"]
            keys_found.add(key)
            history = self.people.get(key)
            if history is None:
                new += 1
                self.people[key] = [(dt, person)]
            elif history[-1][0] > dt:
                log.warn("%s: last seen for %s is from %s but found earlier %s: skipped", fn, key, history[-1][0], dt)
            elif history[-1][1] != person:
                updated += 1
                history.append((dt, person))
            else:
                unchanged += 1

        removed = 0
        for key in self.people.keys() - keys_found:
            history = self.people.get(key)
            if history[-1][1] is not None:
                history.append((dt, None))
                removed += 1

        log.info("%s: %d new, %d updated, %d unchanged %d removed", fn, new, updated, unchanged, removed)

    def print_history(self, key):
        try:
            from jsondiff import JsonDiffer
            differ = JsonDiffer(syntax="explicit")
        except ModuleNotFoundError:
            differ = None

        person = self.people.get(key)
        if person is None:
            log.error("%s: person not found", key)
            return

        last = None
        for dt, data in person:
            print("# {}".format(dt))
            if last is None or differ is None:
                json.dump(data, sys.stdout, indent=1)
                print()
            else:
                d = differ.diff(last, data)
                json.dump(differ.marshal(d), sys.stdout, indent=1)
                print()
            last = data


def main():
    parser = argparse.ArgumentParser(description="Maintain a summary of all changes to the nm.debian.org database")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")
    parser.add_argument("--debug", action="store_true", help="debug output")
    parser.add_argument("-d", "--db", action="store", help="merged JSON data")
    parser.add_argument("--add-dir", action="store", metavar="dir", help="add all JSON files from the given directory")
    parser.add_argument("--keys", action="store_true", help="show all keys known to the summary")
    parser.add_argument("--history", action="store", metavar="key", help="show the history for a key")

    args = parser.parse_args()

    log_format = "%(levelname)s %(message)s"
    level = logging.WARN
    if args.debug:
        level = logging.DEBUG
    elif args.verbose:
        level = logging.INFO
    logging.basicConfig(level=level, stream=sys.stderr, format=log_format)

    summary = Summary()
    if args.db:
        summary.load(args.db)

    modified = False

    if args.add_dir:
        modified = True
        summary.add_dir(args.add_dir)

    if args.keys:
        for key in sorted(summary.people.keys()):
            print(key)

    if args.history:
        summary.print_history(args.history)

    if modified and args.db:
        summary.save(args.db)


if __name__ == "__main__":
    try:
        main()
    except Fail as e:
        print(e, file=sys.stderr)
        sys.exit(1)
    except Exception:
        log.exception("uncaught exception")
