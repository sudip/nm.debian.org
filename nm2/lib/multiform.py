# Copyright © 2020 Truelite S.r.l
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms.utils import ErrorList


class Multiform:
    """
    Form-like class that dispatches to multiple forms.

    Each form gets a different prefix, based on the form name.

    These are the differences between Multiform's constructor arguments and
    Form constructor arguments:

     - data, files: unchanged
     - auto_id, error_class, label_suffix, empty_permitted,
       use_required_attribute, renderer: dispatched as is to all forms by
       default. If they are dict, the form name is looked up as the value. If
       missing, the default Form constructor value is used
     - initial, field_order, instance: if provided, they must be a dict indexed by form name
     - prefix: if missing, the form name is used. If a dict, the form prefix is
       looked up by name, defaulting to the form name. If a string, the form
       name is appended to it to create the form prefix.

    Individual forms can be accessed using the forms dict, and are produced as
    a sequence when the Multiform instance is iterated.

    __getitem__ is implemented to loop up a form by name directly for
    convenience. If a form name conflicts with a Multiform method name, you can
    access it via the forms dict.

    Only is_valid and has_changed are implemented at the Multiform level. For
    all the other form instance methods, you need to access the individual
    forms via the forms dict.
    """
    # Dict of name: Form class
    form_classes = None

    def __init__(
            self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None,
            error_class=ErrorList, label_suffix=None, empty_permitted=False,
            field_order=None, instance=None, use_required_attribute=None, renderer=None):
        forms = {}
        for name, cls in self.form_classes.items():
            args = {
                "data": data,
                "files": files,
            }

            if initial is not None:
                form_initial = initial.get(name)
                if form_initial is not None:
                    args["initial"] = form_initial

            if field_order is not None:
                form_field_order = field_order.get(name)
                if form_field_order is not None:
                    args["field_order"] = form_field_order

            if instance is not None:
                form_instance = instance.get(name)
                if form_instance is not None:
                    args["instance"] = form_instance

            if prefix is None:
                args["prefix"] = name
            elif isinstance(prefix, dict):
                args["prefix"] = prefix.get(name, name)
            else:
                args["prefix"] = prefix + name

            if isinstance(auto_id, dict):
                args["auto_id"] = auto_id.get(name)
            else:
                args["auto_id"] = auto_id

            if isinstance(error_class, dict):
                args["error_class"] = error_class.get(name, ErrorList)
            else:
                args["error_class"] = error_class

            if isinstance(label_suffix, dict):
                args["label_suffix"] = label_suffix.get(name)
            elif label_suffix is not None:
                args["label_suffix"] = label_suffix

            if isinstance(empty_permitted, dict):
                args["empty_permitted"] = empty_permitted.get(name, False)
            elif empty_permitted is not False:
                args["empty_permitted"] = empty_permitted

            if isinstance(use_required_attribute, dict):
                args["use_required_attribute"] = use_required_attribute.get(name)
            elif use_required_attribute is not None:
                args["use_required_attribute"] = use_required_attribute

            if isinstance(renderer, dict):
                args["renderer"] = renderer.get(name)
            elif renderer is not None:
                args["renderer"] = renderer

            forms[name] = cls(**args)

        # The actual forms, indexed by name
        self.forms = forms

    def __iter__(self):
        return self.forms.values()

    def __getitem__(self, name):
        return self.forms[name]

    def is_valid(self):
        return all(f.is_valid() for f in self.forms.values())

    def has_changed(self):
        return any(f.has_changed() for f in self.forms.values())
