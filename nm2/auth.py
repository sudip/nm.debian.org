from django.conf import settings


class AuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.sso_username = None

        # Allow to override the current user via settings for tests
        remote_user = getattr(settings, "TEST_USER", None)
        if remote_user is not None:
            request.META["REMOTE_USER"] = remote_user
            request.sso_username = remote_user
            return self.get_response(request)

        # Get user from SSO certificates
        cert_user = request.META.get("SSL_CLIENT_S_DN_CN", None)
        if cert_user is not None:
            request.META["REMOTE_USER"] = cert_user
            request.sso_username = cert_user
        else:
            request.META.pop("REMOTE_USER", None)

        return self.get_response(request)
