from rest_framework import serializers
import backend.models as bmodels
from process.serializers import ProcessExportSerializer, ProcessRedactedExportSerializer
import legacy.serializers as lserializers
import dsa.serializers as dserializers
from .serializer_fields import PersonKeyField


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = bmodels.Person
        fields = (
            'id', 'username', 'is_staff',
            'fullname', 'email', 'bio',
            'status', 'status_changed',
            'fd_comment', 'fpr',
            'ldap_fields',
        )

    ldap_fields = dserializers.LDAPFieldsSerializer()

    def to_representation(self, instance):
        res = super().to_representation(instance)

        # Remove fields that the given user is not allowed to see
        allowed_fields = {'id', 'fullname', 'bio', 'status', 'status_changed', 'fpr', 'ldap_fields'}

        request = self.context.get("request")
        if request is not None and not request.user.is_anonymous:
            if request.user.is_dd:
                allowed_fields.update(('username', 'is_staff', 'email'))

            if request.user.is_admin:
                allowed_fields.add('fd_comment')

        for field_name in res.keys() - allowed_fields:
            res.pop(field_name)

        return res


class AMExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = bmodels.AM
        exclude = ["id", "person"]


class AMRedactedExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = bmodels.AM
        fields = ["slots", "is_am", "is_fd", "is_dam", "is_am_ctte", "created"]

    def to_representation(self, obj):
        res = super().to_representation(obj)
        res["fd_comment"] = ""
        return res


class PersonAuditLogExportSerializer(serializers.ModelSerializer):
    author = PersonKeyField()

    class Meta:
        model = bmodels.PersonAuditLog
        exclude = ["id", "person"]


class FingerprintExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = bmodels.Fingerprint
        exclude = ["id", "person"]


class FingerprintRedactedExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = bmodels.Fingerprint
        fields = ["fpr", "is_active", "last_upload"]


class PersonExportSerializer(serializers.ModelSerializer):
    ldap_fields = dserializers.LDAPFieldsSerializer()
    am = AMExportSerializer(allow_null=True)
    fprs = FingerprintExportSerializer(many=True, allow_empty=True)
    audit_log = PersonAuditLogExportSerializer(many=True, allow_empty=True)
    legacy_processes = lserializers.ProcessExportSerializer(many=True, allow_empty=True)
    processes = ProcessExportSerializer(many=True, allow_empty=True)

    class Meta:
        model = bmodels.Person
        exclude = ["id", "groups", "user_permissions"]

    def to_representation(self, obj):
        res = super().to_representation(obj)
        res["lookup_key"] = obj.lookup_key
        return res


class PersonRedactedExportSerializer(serializers.ModelSerializer):
    ldap_fields = dserializers.LDAPFieldsSerializer()
    am = AMRedactedExportSerializer(allow_null=True)
    fprs = FingerprintRedactedExportSerializer(many=True)
    legacy_processes = lserializers.ProcessRedactedExportSerializer(many=True)
    processes = ProcessRedactedExportSerializer(many=True, allow_empty=True)

    class Meta:
        model = bmodels.Person
        fields = ["username", "is_staff", "is_superuser", "ldap_fields",
                  "fullname", "bio", "status", "status_changed", "status_description",
                  "created", "expires", "am", "fprs", "legacy_processes", "processes"]

    def to_representation(self, obj):
        res = super().to_representation(obj)
        res["lookup_key"] = obj.lookup_key
        res["email"] = "{}@example.org".format(obj.lookup_key.replace("@", "_"))
        res["ldap_fields"]["email"] = "person-private@example.org"
        res["fd_comment"] = ""
        if obj.pending:
            res["pending"] = "RANDOM_NONCE"
        res["last_vote"] = None
        res["audit_log"] = []
        return res
