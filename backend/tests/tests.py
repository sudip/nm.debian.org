import django
from django.test import TransactionTestCase
import backend.models as bmodels
import legacy.models as lmodels
import backend.const as bconst
import dsa.models as dmodels
import datetime


def dump_message(msg):
    import sys
    print("FROM", msg.from_email, file=sys.stderr)
    print("TO", msg.to, file=sys.stderr)
    print("CC", msg.cc, file=sys.stderr)
    print("BCC", msg.bcc, file=sys.stderr)
    print("SUBJ", msg.subject, file=sys.stderr)
    print("BODY", msg.body, file=sys.stderr)


class SimpleFixture(object):
    def __init__(self):
        self.fd = bmodels.Person(
                username="enrico@debian.org", cn="Enrico", sn="Zini", email="enrico@debian.org",
                uid="enrico", status=bconst.STATUS_DD_U)
        self.fd.save(audit_skip=True)

        self.fd_am = bmodels.AM(person=self.fd, slots=1, is_am=True, is_fd=True, is_dam=True)
        self.fd_am.save()

        self.adv = bmodels.Person(
                username="andrea@debian.org", cn="Andrea", sn="Berardi", email="andrea@debian.org",
                uid="andrea", status=bconst.STATUS_DD_NU)
        self.adv.save(audit_skip=True)

        self.dd = bmodels.Person(
                username="lesley@debian.org", cn="Lesley", sn="Leisel", email="lesley@debian.org",
                uid="lesley", status=bconst.STATUS_DD_U)
        self.dd.save(audit_skip=True)

        self.am = bmodels.Person(
                username="jane@debian.org", cn="Jane", sn="Doe", email="jane@janedoe.org",
                uid="jane", status=bconst.STATUS_DD_U)
        self.am.save(audit_skip=True)

        self.am_am = bmodels.AM(person=self.am, slots=1, is_am=True)
        self.am_am.save()

        self.nm = bmodels.Person(
                username="john-guest@users.alioth.debian.org", cn="John", sn="Smith", email="doctor@example.com",
                status=bconst.STATUS_DC, bio="I meet people, I do things.")
        self.nm.save(audit_skip=True)

    def make_process_dm(self, progress=bconst.PROGRESS_DONE):
        self.process_dm = lmodels.Process(
                person=self.nm, applying_as=bconst.STATUS_DC,
                applying_for=bconst.STATUS_DM, progress=progress,
                manager=self.am_am, is_active=progress == bconst.PROGRESS_DONE)
        self.process_dm.save()
        return self.process_dm

    def make_process_dd(self, progress=bconst.PROGRESS_DONE, advocate=None):
        self.process_dd = lmodels.Process(
                person=self.nm, applying_as=bconst.STATUS_DM,
                applying_for=bconst.STATUS_DD_U, progress=progress,
                manager=self.am_am, is_active=progress != bconst.PROGRESS_DONE)
        self.process_dd.save()
        if advocate is not None:
            self.process_dd.advocates.add(advocate)
        return self.process_dd


class FingerprintTest(TransactionTestCase):
    def test_fpr_field(self):
        from django.db import connection
        p = bmodels.Person.objects.create_user(
                email="test@example.org", fullname="Test", status=bconst.STATUS_DC, audit_skip=True)
        dmodels.LDAPFields.objects.create(person=p, cn="Test", sn="Test", uid="test", audit_skip=True)

        # Verify how fingerprints are stored in the DB
        cr = connection.cursor()

        # Spaces are stripped
        f = bmodels.Fingerprint.objects.create(
                fpr="A410 5B0A 9F84 97EC AB5F  1683 8D5B 478C F7FE 4DAA", person=p, audit_skip=True)
        db_fpr = cr.execute("select fpr from fingerprints where id='{}'".format(f.id)).fetchone()[0]
        self.assertEqual(db_fpr, "A4105B0A9F8497ECAB5F16838D5B478CF7FE4DAA")

        # Letters are uppercased
        f = bmodels.Fingerprint.objects.create(
                fpr="a410 5b0a 9f84 97ec ab5f1683 8d5b 478c f7fe 4dab", person=p, audit_skip=True)
        on_db_valid_fpr = cr.execute("select fpr from fingerprints where id='{}'".format(f.id)).fetchone()[0]
        self.assertEqual(on_db_valid_fpr, "A4105B0A9F8497ECAB5F16838D5B478CF7FE4DAB")

        # Everything else is discarded
        with self.assertRaises(django.db.IntegrityError):
            bmodels.Fingerprint.objects.create(
                    fpr="FIXME: I'll let you know later when I'll have a bit of a clue", person=p, audit_skip=True)

        with self.assertRaises(django.db.IntegrityError):
            bmodels.Fingerprint.objects.create(fpr="", audit_skip=True)


class PersonExpires(TransactionTestCase):
    def create_person(self):
        person = bmodels.Person.objects.create_user(
                fullname="Enrico Zini", email="enrico@debian.org", status=bconst.STATUS_DC, audit_skip=True)
        dmodels.LDAPFields.objects.create(person=person, cn="Enrico", sn="Zini", uid="enrico", audit_skip=True)
        return person

    def run_maint(self):
        from django_housekeeping import Housekeeping
        from backend.housekeeping import BackupDB
        hk = Housekeeping(test_mock=BackupDB)
        hk.autodiscover()
        hk.init()
        with self.assertLogs() as log:
            hk.run(run_filter=lambda name: "PersonExpires" in name)
        for entry in log.output:
            if entry.startswith("ERROR"):
                self.fail(f"run_maint error: {entry}")
        res = bmodels.Person.objects.filter(email="enrico@debian.org")
        if res.exists():
            return res[0]
        else:
            return None

    def test_expires(self):
        today = datetime.date.today()
        person = self.create_person()

        # if expires is Null, it won't expire
        self.assertIsNone(person.expires)
        p = self.run_maint()
        self.assertIsNotNone(p)
        self.assertIsNone(p.expires)

        # if expires is today or later, it hasn't expired yet
        person.expires = today
        person.save(audit_skip=True)
        p = self.run_maint()
        self.assertIsNotNone(p)
        self.assertEqual(p.expires, today)

        person.expires = today + datetime.timedelta(days=1)
        person.save(audit_skip=True)
        p = self.run_maint()
        self.assertIsNotNone(p)
        self.assertEqual(p.expires, today + datetime.timedelta(days=1))

        # if expires is older than today and Person is DC and there are no
        # processes, it expires
        person.expires = today - datetime.timedelta(days=1)
        person.save(audit_skip=True)
        p = self.run_maint()
        self.assertIsNone(p)

        person = self.create_person()

        # if expires is older than today and Person is not DC, then it does not
        # expire, and its 'expires' date is reset
        person.status = bconst.STATUS_DC_GA
        person.save(audit_skip=True)
        p = self.run_maint()
        self.assertIsNotNone(p)
        self.assertIsNone(p.expires)

        # if expires is older than today and Person has open processes, then it
        # does not expire, and its 'expires' date is reset
        person.status = bconst.STATUS_DC
        person.save(audit_skip=True)
        proc = lmodels.Process(
                person=person, applying_as=person.status, applying_for=bconst.STATUS_DC_GA,
                progress=bconst.PROGRESS_APP_NEW, is_active=True)
        proc.save()
        p = self.run_maint()
        self.assertIsNotNone(p)
        self.assertIsNone(p.expires)
