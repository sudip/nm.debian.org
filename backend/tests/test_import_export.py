from django.test import TestCase
from django.utils.timezone import utc
from django.forms.models import model_to_dict
from backend import const
import backend.models as bmodels
import legacy.models as lmodels
import process.models as pmodels
import keyring.models as kmodels
import sitechecks.models as smodels
import dsa.models as dmodels
import datetime
from backend.export import export_db, load_db


class TestImportExport(TestCase):
    def setUp(self):
        # Set up a test fixture.
        # Since we clear the database before importing, we save the foreign
        # keys in a _rels attribute, so we can access them later for
        # comparisons in tests

        self.fd = bmodels.Person.objects.create_superuser(
                email="fd@example.org",
                audit_skip=True,
                fpr="ABCD 1234 DEFA 5678 BCDE 9012 FABC 3456 DEFA 7890",
                username="fd@debian.org",
                fullname="First Middle Last",
                is_staff=True,
                bio="Test bio",
                status=const.STATUS_DD_NU,
                status_changed=datetime.datetime(2019, 8, 7, 6, 5, tzinfo=utc),
                fd_comment="test comment",
                status_description="Test FD",
                last_vote=datetime.date(2019, 1, 2),
        )

        dmodels.LDAPFields.objects.create(
                person=self.fd,
                cn="First",
                mn="Middle",
                sn="Last",
                email="fd-private@example.org",
                uid="fd",
                audit_skip=True,
        )

        self.person = bmodels.Person.objects.create_user(
                email="test@example.org",
                audit_author=self.fd,
                audit_notes="created test user",
                fpr="1234 ABCD 5678 DEFA 9012 BCDE 3456 FABC 7890 DEFA",
                username="test@debian.org",
                fullname="First Middle Last",
                is_staff=False,
                bio="Test bio",
                status=const.STATUS_DD_NU,
                status_changed=datetime.datetime(2019, 8, 7, 6, 5, tzinfo=utc),
                fd_comment="test comment",
                status_description="Test Developer",
                expires=datetime.date(2019, 8, 8),
                pending="TEST TOKEN",
                last_vote=datetime.date(2019, 1, 2),
        )

        dmodels.LDAPFields.objects.create(
                person=self.person,
                cn="First",
                mn="Middle",
                sn="Last",
                email="test-private@example.org",
                uid="test",
                audit_skip=True,
        )

        self.fd_am = bmodels.AM.objects.create(
                person=self.fd,
                slots=3,
                is_am=True,
                is_fd=True,
                is_dam=False,
                is_am_ctte=True,
        )

        self.am = bmodels.AM.objects.create(
                person=self.person,
                slots=3,
                is_am=True,
                is_fd=False,
                is_dam=False,
                is_am_ctte=True,
                fd_comment="test comment",
        )

        self.fingerprint = self.person.fingerprint

        self.legacy_process = lmodels.Process.objects.create(
                person=self.person,
                applying_as=const.STATUS_DM,
                applying_for=const.STATUS_DD_NU,
                progress=const.PROGRESS_DONE,
                manager=self.fd_am,
                is_active=False,
                closed=datetime.datetime(2017, 6, 5, 4, 3, tzinfo=utc),
                archive_key="akey")
        self.legacy_process.advocates.set((self.fd,))

        self.legacy_log = lmodels.Log.objects.create(
                changed_by=self.fd,
                process=self.legacy_process,
                progress=const.PROGRESS_DONE,
                is_public=True,
                logdate=self.legacy_process.closed,
                logtext="ok")
        self.legacy_process._rels = {
            "manager": self.fd_am,
            "advocates": [self.fd],
            "log": [self.legacy_log],
        }
        self.legacy_log._rels = {
            "changed_by": self.fd,
        }

        self.person_audit_log = list(bmodels.PersonAuditLog.objects.all())
        for pal in self.person_audit_log:
            pal._rels = {
                "author": pal.author,
            }

        self.process = pmodels.Process(
            person=self.person,
            applying_for=const.STATUS_DD_U,
            started=datetime.datetime(2019, 8, 7, 6, 5, tzinfo=utc),
            frozen_by=self.fd,
            frozen_time=datetime.datetime(2019, 8, 8, 6, 5, tzinfo=utc),
            approved_by=self.fd,
            approved_time=datetime.datetime(2019, 8, 8, 7, 6, tzinfo=utc),
            fd_comment="test fd comment",
            rt_request="please do the thing",
            rt_ticket=123,
            hide_until=datetime.datetime(2019, 8, 9, 1, 2, tzinfo=utc))
        self.process.save()

        self.requirement = pmodels.Requirement.objects.create(
            process=self.process,
            type="intent",
            approved_by=self.fd,
            approved_time=datetime.datetime(2019, 8, 7, 8, 7, tzinfo=utc))

        self.statement = pmodels.Statement.objects.create(
            requirement=self.requirement,
            fpr=self.person.fingerprint,
            statement="test statement",
            uploaded_by=self.person,
            uploaded_time=datetime.datetime(2019, 8, 7, 6, 7, tzinfo=utc))

        self.amassignment = pmodels.AMAssignment.objects.create(
            process=self.process,
            am=self.fd_am,
            assigned_by=self.fd,
            assigned_time=datetime.datetime(2019, 8, 7, 9, 8, tzinfo=utc),
            unassigned_by=self.fd,
            unassigned_time=datetime.datetime(2019, 8, 7, 9, 9, tzinfo=utc))

        self.process_log = pmodels.Log.objects.create(
            changed_by=self.fd,
            process=self.process,
            is_public=False,
            logdate=datetime.datetime(2019, 8, 7, 6, 6, tzinfo=utc),
            action="test",
            logtext="test process log")

        self.requirement_log = pmodels.Log.objects.create(
            changed_by=self.person,
            process=self.process,
            requirement=self.requirement,
            is_public=True,
            logdate=datetime.datetime(2019, 8, 7, 6, 7, tzinfo=utc),
            action="statement",
            logtext="test requirement log")

        self.process._rels = {
            "requirements": [self.requirement],
            "log": [self.requirement_log, self.process_log],
            "ams": [self.amassignment],
            "frozen_by": self.process.frozen_by,
            "approved_by": self.process.approved_by,
            "closed_by": self.process.closed_by,
        }

        self.requirement._rels = {
            "approved_by": self.requirement.approved_by,
            "statements": [self.statement],
        }

        self.statement._rels = {
            "fpr": self.statement.fpr,
            "uploaded_by": self.statement.uploaded_by,
        }

        self.amassignment._rels = {
            "am": self.amassignment.am,
            "assigned_by": self.amassignment.assigned_by,
            "unassigned_by": self.amassignment.unassigned_by,
        }

        self.process_log._rels = {
            "changed_by": self.fd,
            "requirement": None,
        }

        self.requirement_log._rels = {
            "changed_by": self.person,
            "requirement": self.requirement,
        }

        self.key = kmodels.Key.objects.create(
            fpr=self.fingerprint,
            key="GPG KEY",
            key_updated=datetime.datetime(2019, 8, 7, 6, 5, tzinfo=utc),
            check_sigs="test check sigs",
            check_sigs_updated=datetime.datetime(2019, 8, 7, 6, 6, tzinfo=utc),
        )
        self.key._rels = {
            "fpr": self.fingerprint,
        }

        self.inconsistency = smodels.Inconsistency.objects.create(
            person=self.person,
            process=self.process,
            first_seen=datetime.date(2019, 1, 1),
            last_seen=datetime.date(2019, 8, 7),
            ignore_until=datetime.date(2019, 9, 8),
            tag="test tag",
            text="test text",
        )
        self.inconsistency._rels = {
            "person": self.inconsistency.person,
            "process": self.inconsistency.process,
        }

    def assertPersonEqual(self, expected, actual):
        if expected is None and actual is None:
            return
        self.maxDiff = None
        exclude = ["id", "date_joined"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

    def assertRedactedPersonEqual(self, orig, redacted):
        if orig is None and redacted is None:
            return
        self.maxDiff = None
        exclude = ["id", "date_joined", "last_login", "email", "email_ldap", "fd_comment", "last_vote", "pending"]
        self.assertEqual(redacted.email, "{}@example.org".format(orig.lookup_key.replace("@", "_")))
        self.assertEqual(redacted.fd_comment, "")
        self.assertIsNone(redacted.last_vote)
        if orig.pending:
            self.assertEqual(redacted.pending, "RANDOM_NONCE")
        else:
            self.assertFalse(redacted.pending)
        self.assertEqual(
                model_to_dict(orig, exclude=exclude),
                model_to_dict(redacted, exclude=exclude))

    def assertReadactedLDAPFieldsEqual(self, orig, redacted):
        self.assertEqual(redacted.ldap_fields.email, "person-private@example.org")
        self.assertEqual(redacted.ldap_fields.cn, orig.ldap_fields.cn)
        self.assertEqual(redacted.ldap_fields.mn, orig.ldap_fields.mn)
        self.assertEqual(redacted.ldap_fields.sn, orig.ldap_fields.sn)
        self.assertEqual(redacted.ldap_fields.uid, orig.ldap_fields.uid)

    def assertAMEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "created", "person"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

    def assertRedactedAMEqual(self, orig, redacted):
        self.maxDiff = None
        exclude = ["id", "created", "person", "fd_comment"]
        self.assertEqual(redacted.fd_comment, "")
        self.assertEqual(
                model_to_dict(orig, exclude=exclude),
                model_to_dict(redacted, exclude=exclude))

    def assertFingerprintEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "person"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

    def assertPersonAuditLogEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "person", "author"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))
        self.assertPersonEqual(expected._rels["author"], actual._rels["author"])

    def assertLegacyProcessEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "person", "manager", "advocates"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

        self.assertAMEqual(expected._rels["manager"], actual._rels["manager"])
        self.assertEqual(len(actual._rels["advocates"]), 1)
        self.assertPersonEqual(expected._rels["advocates"][0], actual._rels["advocates"][0])

        self.assertEqual(len(actual._rels["log"]), 1)
        self.assertLegacyProcessLogEqual(expected._rels["log"][0], actual._rels["log"][0])

    def assertRedactedLegacyProcessEqual(self, orig, redacted):
        self.maxDiff = None
        exclude = ["id", "person", "manager", "advocates"]
        self.assertEqual(
                model_to_dict(orig, exclude=exclude),
                model_to_dict(redacted, exclude=exclude))

        self.assertRedactedAMEqual(orig._rels["manager"], redacted._rels["manager"])
        self.assertEqual(len(redacted._rels["advocates"]), 1)
        self.assertRedactedPersonEqual(orig._rels["advocates"][0], redacted._rels["advocates"][0])

        self.assertEqual(redacted._rels["log"], [])

    def assertLegacyProcessLogEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "changed_by", "process"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

        self.assertPersonEqual(expected._rels["changed_by"], actual._rels["changed_by"])

    def assertProcessEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "person", "frozen_by", "approved_by", "closed_by"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

        self.assertPersonEqual(expected._rels["frozen_by"], actual._rels["frozen_by"])
        self.assertPersonEqual(expected._rels["approved_by"], actual._rels["approved_by"])
        self.assertPersonEqual(expected._rels["closed_by"], actual._rels["closed_by"])

        self.assertEqual(len(actual._rels["requirements"]), 1)
        self.assertRequirementEqual(expected._rels["requirements"][0], actual._rels["requirements"][0])

        self.assertEqual(len(actual._rels["ams"]), 1)
        self.assertAMAssignmentEqual(expected._rels["ams"][0], actual._rels["ams"][0])

        self.assertEqual(len(actual._rels["log"]), 2)
        self.assertProcessLogEqual(expected._rels["log"][0], actual._rels["log"][0])
        self.assertProcessLogEqual(expected._rels["log"][1], actual._rels["log"][1])

    def assertRedactedProcessEqual(self, orig, redacted):
        self.maxDiff = None
        exclude = ["id", "person", "frozen_by", "approved_by", "closed_by", "fd_comment", "rt_request", "rt_ticket"]
        self.assertEqual(
                model_to_dict(orig, exclude=exclude),
                model_to_dict(redacted, exclude=exclude))

        self.assertRedactedPersonEqual(orig._rels["frozen_by"], redacted._rels["frozen_by"])
        self.assertRedactedPersonEqual(orig._rels["approved_by"], redacted._rels["approved_by"])
        self.assertRedactedPersonEqual(orig._rels["closed_by"], redacted._rels["closed_by"])

        self.assertFalse(redacted.fd_comment)
        self.assertFalse(redacted.rt_request)
        self.assertFalse(redacted.rt_ticket)

        self.assertEqual(len(redacted._rels["requirements"]), 1)
        self.assertRedactedRequirementEqual(orig._rels["requirements"][0], redacted._rels["requirements"][0])

        self.assertEqual(len(redacted._rels["ams"]), 1)
        self.assertRedactedAMAssignmentEqual(orig._rels["ams"][0], redacted._rels["ams"][0])

        self.assertEqual(len(redacted._rels["log"]), 0)

    def assertRequirementEqual(self, expected, actual):
        if expected is None and actual is None:
            return
        self.maxDiff = None
        exclude = ["id", "process", "approved_by"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

        self.assertPersonEqual(expected._rels["approved_by"], actual._rels["approved_by"])

        self.assertEqual(len(actual._rels["statements"]), 1)
        self.assertStatementEqual(expected._rels["statements"][0], actual._rels["statements"][0])

    def assertRedactedRequirementEqual(self, orig, redacted):
        if orig is None and redacted is None:
            return
        self.maxDiff = None
        exclude = ["id", "process", "approved_by"]
        self.assertEqual(
                model_to_dict(orig, exclude=exclude),
                model_to_dict(redacted, exclude=exclude))

        self.assertRedactedPersonEqual(orig._rels["approved_by"], redacted._rels["approved_by"])

        self.assertEqual(len(redacted._rels["statements"]), 1)
        self.assertRedactedStatementEqual(orig._rels["statements"][0], redacted._rels["statements"][0])

    def assertAMAssignmentEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "process", "am", "assigned_by", "unassigned_by"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

        self.assertAMEqual(expected._rels["am"], actual._rels["am"])
        self.assertPersonEqual(expected._rels["assigned_by"], actual._rels["assigned_by"])
        self.assertPersonEqual(expected._rels["unassigned_by"], actual._rels["unassigned_by"])

    def assertRedactedAMAssignmentEqual(self, orig, redacted):
        self.maxDiff = None
        exclude = ["id", "process", "am", "assigned_by", "unassigned_by"]
        self.assertEqual(
                model_to_dict(orig, exclude=exclude),
                model_to_dict(redacted, exclude=exclude))

        self.assertRedactedAMEqual(orig._rels["am"], redacted._rels["am"])
        self.assertRedactedPersonEqual(orig._rels["assigned_by"], redacted._rels["assigned_by"])
        self.assertRedactedPersonEqual(orig._rels["unassigned_by"], redacted._rels["unassigned_by"])

    def assertStatementEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "requirement", "fpr", "uploaded_by"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

        self.assertPersonEqual(expected._rels["uploaded_by"], actual._rels["uploaded_by"])
        self.assertFingerprintEqual(expected._rels["fpr"], actual._rels["fpr"])

    def assertRedactedStatementEqual(self, orig, redacted):
        self.maxDiff = None
        exclude = ["id", "requirement", "fpr", "uploaded_by"]
        self.assertEqual(
                model_to_dict(orig, exclude=exclude),
                model_to_dict(redacted, exclude=exclude))

        self.assertRedactedPersonEqual(orig._rels["uploaded_by"], redacted._rels["uploaded_by"])
        self.assertFingerprintEqual(orig._rels["fpr"], redacted._rels["fpr"])

    def assertProcessLogEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "process", "changed_by", "requirement"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))

        self.assertPersonEqual(expected._rels["changed_by"], actual._rels["changed_by"])
        self.assertRequirementEqual(expected._rels["requirement"], actual._rels["requirement"])

    def assertGpgKeyEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "fpr"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))
        self.assertFingerprintEqual(expected._rels["fpr"], actual._rels["fpr"])

    def assertInconsistencyEqual(self, expected, actual):
        self.maxDiff = None
        exclude = ["id", "person", "process"]
        self.assertEqual(
                model_to_dict(expected, exclude=exclude),
                model_to_dict(actual, exclude=exclude))
        self.assertPersonEqual(expected._rels["person"], actual._rels["person"])
        self.assertProcessEqual(expected._rels["process"], actual._rels["process"])

    def clear_database(self):
        pmodels.Process.objects.all().delete()
        lmodels.Process.objects.all().delete()
        bmodels.Person.objects.all().delete()
        kmodels.Key.objects.all().delete()
        smodels.Inconsistency.objects.all().delete()

    def assertSaves(self, loaded):
        """
        Check that the importer saves objects correctly
        """
        loaded.save()
        person = loaded.people["test"]
        fd = loaded.people["fd"]
        self.assertIsInstance(person.pk, int)
        self.assertIsInstance(loaded.ams["test"].pk, int)
        self.assertEqual(loaded.ams["test"].person, person)
        self.assertEqual(loaded.fprs[self.fingerprint.fpr].person, person)
        for al in person.audit_log.all():
            self.assertIsInstance(al.pk, int)
            self.assertEqual(al.author, fd)

        legacy_process = lmodels.Process.objects.get(person=person)
        self.assertIsInstance(legacy_process.pk, int)
        self.assertEqual(legacy_process.manager, loaded.ams["fd"])
        self.assertEqual(legacy_process.advocates.get(), fd)
        for log in legacy_process.log.all():
            self.assertIsInstance(log.pk, int)
            self.assertEqual(log.changed_by, fd)

        process = person.processes.first()
        self.assertIsInstance(process.pk, int)
        self.assertEqual(process.frozen_by, fd)
        self.assertEqual(process.approved_by, fd)
        self.assertEqual(process.closed_by, None)

        requirement = process.requirements.first()
        self.assertIsInstance(requirement.pk, int)
        self.assertEqual(requirement.approved_by, fd)

        amassignment = process.ams.first()
        self.assertIsInstance(amassignment.pk, int)
        self.assertEqual(amassignment.am, loaded.ams["fd"])
        self.assertEqual(amassignment.assigned_by, fd)
        self.assertEqual(amassignment.unassigned_by, fd)

        statement = requirement.statements.first()
        self.assertIsInstance(statement.pk, int)
        self.assertEqual(statement.fpr, person.fingerprint)
        self.assertEqual(statement.uploaded_by, person)

        log = list(process.log.all())
        if log:
            self.assertIsInstance(log[0].pk, int)
            self.assertEqual(log[0].changed_by, person)
            self.assertEqual(log[0].requirement, requirement)
            self.assertIsInstance(log[1].pk, int)
            self.assertEqual(log[1].changed_by, fd)
            self.assertIsNone(log[1].requirement)

        if loaded.gpgkeys:
            key = loaded.gpgkeys[0]
            self.assertIsInstance(key.pk, int)
            self.assertEqual(key.fpr, person.fingerprint)

        if loaded.inconsistencies:
            inconsistency = loaded.inconsistencies[0]
            self.assertIsInstance(inconsistency.pk, int)
            self.assertEqual(inconsistency.person, person)
            self.assertEqual(inconsistency.process, process)

    def test_redacted(self):
        serialized = export_db(full=False)
        self.clear_database()
        loaded = load_db(serialized)
        person = loaded.people["test"]
        self.assertRedactedPersonEqual(self.person, person)
        self.assertRedactedAMEqual(self.am, loaded.ams["test"])
        self.assertFingerprintEqual(self.fingerprint, loaded.fprs[self.fingerprint.fpr])
        self.assertEqual(person._rels["audit_log"], [])

        self.assertEqual(len(person._rels["legacy_processes"]), 1)
        self.assertRedactedLegacyProcessEqual(self.legacy_process, person._rels["legacy_processes"][0])

        self.assertRedactedProcessEqual(self.process, person._rels["processes"][0])

        self.assertEqual(len(loaded.gpgkeys), 0)
        self.assertEqual(len(loaded.inconsistencies), 0)

        self.assertSaves(loaded)

        self.assertReadactedLDAPFieldsEqual(self.person, person)

    def test_full(self):
        serialized = export_db(full=True)
        self.clear_database()
        loaded = load_db(serialized)
        person = loaded.people["test"]
        self.assertPersonEqual(self.person, person)
        self.assertAMEqual(self.am, loaded.ams["test"])
        self.assertFingerprintEqual(self.fingerprint, loaded.fprs[self.fingerprint.fpr])

        audit_log = person._rels["audit_log"]
        self.assertPersonAuditLogEqual(audit_log[0], self.person_audit_log[0])
        self.assertPersonAuditLogEqual(audit_log[1], self.person_audit_log[1])
        self.assertEqual(len(audit_log), 2)

        self.assertEqual(len(person._rels["legacy_processes"]), 1)
        self.assertLegacyProcessEqual(self.legacy_process, person._rels["legacy_processes"][0])

        self.assertProcessEqual(self.process, person._rels["processes"][0])

        self.assertEqual(len(loaded.gpgkeys), 1)
        self.assertGpgKeyEqual(loaded.gpgkeys[0], self.key)

        self.assertEqual(len(loaded.inconsistencies), 1)
        self.assertInconsistencyEqual(loaded.inconsistencies[0], self.inconsistency)

        self.assertSaves(loaded)
