from __future__ import annotations
from typing import TYPE_CHECKING, BinaryIO, List, Dict, Any
from django.utils.timezone import utc
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
import datetime
import email
import email.utils
try:
    from email.Iterators import typed_subpart_iterator
except ImportError:
    from email.iterators import typed_subpart_iterator
import mailbox
import lzma
import gzip
from contextlib import contextmanager
import logging

if TYPE_CHECKING:
    import email.message

log = logging.getLogger(__name__)

EMAIL_PRIVATE_ANNOUNCES = getattr(settings, "EMAIL_PRIVATE_ANNOUNCES", "nm@debian.org")


class StoredEmail:
    def __init__(self, message: email.message.EmailMessage):
        self.message = message
        self.headers = {
                name: self._decode_header(val)
                for name, val in self.message.items()
            }
        date = self.message.get("Date")
        if not date:
            self.date = None
        else:
            self.date = email.utils.parsedate_to_datetime(self.message["Date"])
        if self.date and self.date.tzinfo is None:
            self.date = self.date.replace(tzinfo=utc)

    def get_body(self) -> str:
        """
        Get the body of the email message
        """
        if self.message.is_multipart():
            # get the plain text version only
            text_parts = [part
                          for part in typed_subpart_iterator(
                              self.message, 'text', 'plain')]
            body = []
            for part in text_parts:
                charset = get_charset(part, get_charset(self.message))
                body.append(str(part.get_payload(decode=True),
                                charset,
                                "replace"))
            return "\n".join(body).strip()
        else:
            # if it is not multipart, the payload will be a string representing the message body
            body = str(self.message.get_payload(decode=True),
                       get_charset(self.message),
                       "replace")
            return body.strip()

    @classmethod
    def get_mbox(cls, filename) -> List["StoredEmail"]:
        res = []
        try:  # we are reading, have not to flush with close
            with get_mbox(filename) as messages:
                for message in messages:
                    res.append(cls(message))
        except mailbox.NoSuchMailboxError:
            return res
        epoch = datetime.datetime(1970, 1, 1, 0, 0, tzinfo=utc)
        res.sort(key=lambda x: x.date if x.date is not None else epoch)
        return res

    @classmethod
    def get_mbox_jsonable(cls, filename) -> List[Dict[str, Any]]:
        res = []
        for msg in cls.get_mbox(filename):
            res.append({
                "from": msg.headers.get("From"),
                "date_rfc": msg.headers.get("Date"),
                "date_iso": msg.date.isoformat() if msg.date else None,
                "subject": msg.headers.get("Subject"),
                "body": msg.get_body(),
            })
        return res

    @classmethod
    def _decode_header(cls, val):
        res = []
        for buf, charset in email.header.decode_header(val):
            if charset is None:
                if isinstance(buf, bytes):
                    res.append(buf.decode("utf-8", errors="replace"))
                else:
                    res.append(buf)
            elif charset == "unknown-8bit":
                res.append(buf.decode("utf-8", errors="replace"))
            else:
                res.append(buf.decode(charset, errors="replace"))
        return " ".join(res)


def get_charset(message, default="ascii"):
    """Get the message charset"""

    if message.get_content_charset():
        return message.get_content_charset()

    if message.get_charset():
        return message.get_charset()

    return default


def parse_recipient_list(s):
    """
    Parse a string like "Foo <a@b.c>, bar@example.com"
    and return a list like ["Foo <a@b.c>", "bar@example.com"]
    """
    res = []
    for name, addr in email.utils.getaddresses([s]):
        res.append(email.utils.formataddr((name, addr)))
    return res


def template_to_email(template_name, context):
    """
    Render a template with its context, parse the result and build an
    EmailMessage from it.
    """
    context.setdefault("default_from", "nm@debian.org")
    context.setdefault("default_subject", "Notification from nm.debian.org")
    text = render_to_string(template_name, context).strip()
    m = email.message_from_string(text)
    msg = EmailMessage()
    msg.from_email = m.get("From", context["default_from"])
    msg.to = parse_recipient_list(m.get("To", EMAIL_PRIVATE_ANNOUNCES))
    if "Cc" in m:
        msg.cc = parse_recipient_list(m.get("Cc"))
    if "Bcc" in m:
        msg.bcc = parse_recipient_list(m.get("Bcc"))
    rt = m.get("Reply-To", None)
    if rt is not None:
        msg.extra_headers["Reply-To"] = rt
    msg.subject = m.get("Subject", context["default_subject"])
    msg.body = m.get_payload()
    return msg


def send_notification(template_name, log_next, log_prev=None):
    """
    Render a notification email template for a transition from log_prev to log,
    then send the resulting email.
    """
    try:
        ctx = {
            "process": log_next.process,
            "log": log_next,
            "log_prev": log_prev,
            "default_subject": "Notification from nm.debian.org",
        }
        if log_next.changed_by is not None:
            ctx["default_from"] = log_next.changed_by.preferred_email
        msg = template_to_email(template_name, ctx)
        msg.send()
        log.debug("sent mail from %s to %s cc %s bcc %s subject %s",
                  msg.from_email,
                  ", ".join(msg.to),
                  ", ".join(msg.cc),
                  ", ".join(msg.bcc),
                  msg.subject)
    except Exception:
        log.exception("failed to sent mail for log %s", log_next)


def send_nonce(template_name, person, nonce=None, encrypted_nonce=None):
    """
    Render an email template to send a nonce to a person,
    then send the resulting email.
    """
    if nonce is None:
        nonce = person.pending
    try:
        ctx = {
            "person": person,
            "nonce": nonce,
            "encrypted_nonce": encrypted_nonce,
            "reply_to": "nm@debian.org",
            "default_subject": "Confirmation from nm.debian.org",
        }
        msg = template_to_email(template_name, ctx)
        msg.send()
        log.debug("sent mail from %s to %s cc %s bcc %s subject %s",
                  msg.from_email,
                  ", ".join(msg.to),
                  ", ".join(msg.cc),
                  ", ".join(msg.bcc),
                  msg.subject)
    except Exception:
        log.exception("failed to sent mail for person %s", person)


# See https://www.enricozini.org/blog/2019/debian/python-hacks-opening-a-compressed-mailbox/
class StreamMbox(mailbox.mbox):
    """
    mailbox.mbox does not support opening a stream, which is sad.

    This is a subclass that works around it
    """
    def __init__(self, fd: BinaryIO, factory=None, create: bool = True):
        # Do not call parent __init__, just redo everything here to be able to
        # open a stream. This will need to be re-reviewed for every new version
        # of python's stdlib.

        # Mailbox constructor
        self._path = None
        self._factory = factory

        # _singlefileMailbox constructor
        self._file = fd
        self._toc = None
        self._next_key = 0
        self._pending = False       # No changes require rewriting the file.
        self._pending_sync = False  # No need to sync the file
        self._locked = False
        self._file_length = None    # Used to record mailbox size

        # mbox constructor
        self._message_factory = mailbox.mboxMessage

    def flush(self):
        raise NotImplementedError("StreamMbox is a readonly class")


@contextmanager
def get_mbox(filename):
    if filename.endswith(".gz"):
        with gzip.open(filename) as fd:
            yield StreamMbox(fd)
    elif filename.endswith(".xz"):
        with lzma.open(filename) as fd:
            yield StreamMbox(fd)
    else:
        yield mailbox.mbox(filename, create=False)
