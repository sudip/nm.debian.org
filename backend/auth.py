from django import http
from django.shortcuts import redirect
import backend.models as bmodels
from django.contrib.auth.backends import RemoteUserBackend


class NMUserBackend(RemoteUserBackend):
    """
    RemoteUserBackend customised to create User objects from Person
    """

    # Copied from RemoteUserBackend and tweaked to validate against Person
    def authenticate(self, request, remote_user):
        """
        The username passed as ``remote_user`` is considered trusted.  This
        method simply returns the ``User`` object with the given username,
        creating a new ``User`` object if ``create_unknown_user`` is ``True``.

        Returns None if ``create_unknown_user`` is ``False`` and a ``User``
        object with the given username is not found in the database.
        """
        if not remote_user:
            return
        username = self.clean_username(remote_user)

        # Get the Person for this username: Person is authoritative over User
        # Allow user@alioth without -guest, for cases like retired DDs who are
        # DMs (Edward Betts <edward> is an example)
        if username.endswith("@debian.org") or username.endswith("@users.alioth.debian.org"):
            try:
                return bmodels.Person.objects.get(username=username)
            except bmodels.Person.DoesNotExist:
                return None
        else:
            return None
