# Generated by Django 2.2.9 on 2020-02-24 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0005_auto_20200223_1726'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='fullname',
            field=models.CharField(default='', max_length=255, verbose_name='full name'),
            preserve_default=False,
        ),
    ]
