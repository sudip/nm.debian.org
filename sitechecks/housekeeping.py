from collections import defaultdict
import logging
import django_housekeeping as hk
from backend.housekeeping import MakeLink
from django.utils import timezone
from django.urls import reverse
from django.contrib.sites.shortcuts import get_current_site
from sitechecks.models import Inconsistency

log = logging.getLogger(__name__)

STAGES = ["init", "backup", "stats"]


class ReportInconsistencies(hk.Task):
    """
    Backup of the whole database
    """
    DEPENDS = [MakeLink]

    def run_init(self, stage):
        self.start_date = timezone.now().date()

    def run_stats(self, stage):
        # Group current inconsistencies by person
        by_person = defaultdict(list)
        for i in Inconsistency.objects.filter(last_seen__gte=self.start_date):
            if i.ignore_until and i.ignore_until >= self.start_date:
                continue
            if i.person:
                by_person[i.person].append(i)
            else:
                by_person[None].append(i)

        if not by_person:
            return

        def log_inconsistency(i):
            if i.process:
                log.warn("%s: %s: %s (%s)",
                         i.first_seen.strftime("%Y-%m-%d"), i.tag, i.text, self.hk.link(i.process))
            else:
                log.warn("%s: %s: %s", i.first_seen.strftime("%Y-%m-%d"), i.tag, i.text)

        log.warn("Active inconsistency after this maintenance run:")
        if by_person.get(None):
            log.warn("General inconsistencies:")
            for i in by_person[None]:
                log_inconsistency(i)
            del by_person[None]
        for person, inconsistencies in sorted(by_person.items(), key=lambda p: p[0].lookup_key):
            log.warn("%s:", self.hk.link(person))
            for i in inconsistencies:
                log_inconsistency(i)

        site = get_current_site(None)
        url = reverse("sitechecks_list")
        if site.domain == "localhost":
            url = "http://localhost:8000" + url
        else:
            url = "https://{}{}".format(site.domain, url)
        log.warn("Manage inconsistencies at %s", url)

        # Delete old inconsistencies that are not current anymore
        Inconsistency.objects.filter(last_seen__lt=self.start_date).delete()
