# Generated by Django 2.2.9 on 2020-02-24 10:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('legacy', '0003_auto_20200223_1725'),
    ]

    operations = [
        migrations.AlterField(
            model_name='process',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='legacy_processes', to=settings.AUTH_USER_MODEL),
        ),
    ]
