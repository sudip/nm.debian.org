from rest_framework import serializers
import legacy.models as lmodels
from backend.serializer_fields import PersonKeyField, AMKeyField


class LogExportSerializer(serializers.ModelSerializer):
    changed_by = PersonKeyField(allow_null=True)

    class Meta:
        model = lmodels.Log
        exclude = ["id", "process"]


class ProcessExportSerializer(serializers.ModelSerializer):
    log = LogExportSerializer(many=True)
    advocates = PersonKeyField(many=True)
    manager = AMKeyField(allow_null=True)

    class Meta:
        model = lmodels.Process
        exclude = ["id", "person"]


class ProcessRedactedExportSerializer(serializers.ModelSerializer):
    advocates = PersonKeyField(many=True)
    manager = AMKeyField()

    class Meta:
        model = lmodels.Process
        fields = ["applying_as", "applying_for", "progress", "manager", "advocates",
                  "is_active", "closed", "archive_key"]

    def to_representation(self, obj):
        res = super().to_representation(obj)
        res["log"] = []
        return res
