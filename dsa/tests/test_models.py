from django.test import TestCase
from django.core.exceptions import ValidationError
from backend.models import Person
from dsa.models import LDAPFields
import django.db


class TestLDAPFields(TestCase):
    def test_uid(self):
        person = Person.objects.create_user(fullname="Test", email="test@example.org", audit_skip=True)
        f = LDAPFields.objects.create(person=person, cn="Test", audit_skip=True)
        f.uid = "foo"
        f.full_clean()
        f.save(audit_skip=True)
        f.uid = "foo-guest"
        with self.assertRaises(ValidationError):
            f.full_clean()

        f.uid = "bo"
        with self.assertRaises(ValidationError):
            f.full_clean()

        f.uid = "fooBar"
        with self.assertRaises(ValidationError):
            f.full_clean()

        f.uid = "foo.bar"
        with self.assertRaises(ValidationError):
            f.full_clean()

        # Test duplicate checks on uid
        self.assertEqual(LDAPFields.objects.filter(uid="foo").count(), 1)

        person1 = Person.objects.create_user(fullname="Test1", email="test1@example.org", username="test1@example.org", audit_skip=True)
        f1 = LDAPFields.objects.create(person=person1, cn="Test1", audit_skip=True)
        f1.uid = "foo"
        with self.assertRaises(ValidationError):
            f1.full_clean()
        with django.db.transaction.atomic():
            with self.assertRaises(django.db.utils.IntegrityError):
                f1.save(audit_skip=True)

        self.assertEqual(LDAPFields.objects.filter(uid="foo").count(), 1)
