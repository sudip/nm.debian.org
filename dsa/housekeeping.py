import django_housekeeping as hk
from django.db import transaction
from backend.housekeeping import MakeLink, Housekeeper
from . import udldap
from backend import const
import backend.models as bmodels
import process.models as pmodels
import backend.ops as bops
import process.ops as pops
import dsa.models as dmodels
import logging

log = logging.getLogger(__name__)


class NewGuestAccountsFromDSA(hk.Task):
    """
    Create new Person entries for guest accounts created by DSA
    """
    DEPENDS = [MakeLink, Housekeeper]

    @transaction.atomic
    def run_main(self, stage):
        for entry in udldap.list_people():
            # Skip DDs
            if entry.is_dd and entry.single("keyFingerPrint") is not None:
                continue

            fpr = entry.single("keyFingerPrint")

            # Skip people without fingerprints
            if fpr is None:
                continue

            email = entry.single("emailForward")

            # Skip entries without emails (happens when running outside of the Debian network)
            if email is None:
                continue

            # Find the corresponding person in our database
            person = bmodels.Person.objects.get_from_other_db(
                "LDAP",
                uid=entry.uid,
                fpr=fpr,
                email=email,
                format_person=self.hk.link,
            )

            if not person:
                # New DC_GA
                audit_notes = "created new guest account entry from LDAP"
                person = bmodels.Person.objects.create_user(
                    cn=entry.single("cn"),
                    mn=entry.single("mn") or "",
                    sn=entry.single("sn") or "",
                    email=email,
                    email_ldap=email,
                    uid=entry.uid,
                    fpr=fpr,
                    status=const.STATUS_DC_GA,
                    username="{}@invalid.example.org".format(entry.uid),
                    audit_author=self.hk.housekeeper.user,
                    audit_notes=audit_notes,
                )
                log.warn("%s: %s %s", self.IDENTIFIER, self.hk.link(person), audit_notes)
            else:
                # Validate fields
                if person.ldap_fields.uid is not None and person.ldap_fields.uid != entry.uid:
                    log.warn("%s: LDAP has uid %s for person %s, but uid is %s in our database",
                             self.IDENTIFIER, entry.uid, self.hk.link(person), person.ldap_fields.uid)
                    continue

                if person.fpr is not None and person.fpr != fpr:
                    log.warn("%s: LDAP has fingerprint %s for person %s, but fingerprint is %s in our database",
                             self.IDENTIFIER, fpr, self.hk.link(person), person.fpr)
                    continue

                audit_notes = ["entry found in LDAP"]

                # Ignore differences in email forward: they are caught by
                # CheckLDAPConsistency

                if person.status in (const.STATUS_DC_GA, const.STATUS_DM_GA):
                    # We already know about it: nothing to do
                    pass
                elif person.status in (const.STATUS_DC, const.STATUS_DM):
                    if person.status == const.STATUS_DM:
                        # DM that becomes DM_GA (acquires uid)
                        new_status = const.STATUS_DM_GA
                    else:
                        # DC that becomes DC_GA (acquires uid)
                        new_status = const.STATUS_DC_GA
                    audit_notes = "entry found in LDAP, adding 'guest account' status"

                    try:
                        process = pmodels.Process.objects.get(
                                person=person, closed_by__isnull=True, applying_for=new_status)
                    except pmodels.Process.DoesNotExist:
                        process = None

                    if process is None:
                        op = bops.ChangeStatus(
                            audit_author=self.hk.housekeeper.user, audit_notes=audit_notes,
                            person=person, status=new_status)
                        op.execute()
                    else:
                        op = pops.ProcessClose(
                            audit_author=self.hk.housekeeper.user, audit_notes=audit_notes,
                            process=process,
                        )
                        op.execute()

                    log.info("%s: %s %s", self.IDENTIFIER, self.hk.link(person), audit_notes)
                else:
                    # New uid on a status that is not supposed to have one:
                    # just warn about it
                    log.warn("%s: LDAP has new uid %s for person %s, which already has status %s in our database",
                             self.IDENTIFIER, entry.uid, self.hk.link(person), const.ALL_STATUS_DESCS[person.status])


class CheckLDAPConsistency(hk.Task):
    """
    Show entries that do not match between LDAP and our DB
    """
    DEPENDS = [MakeLink, Housekeeper]

    def run_main(self, stage):
        from backend.models import _build_fullname

        # Prefetch people and index them by uid
        people_by_uid = dict()
        for p in bmodels.Person.objects.all():
            if p.ldap_fields.uid is None:
                continue
            people_by_uid[p.ldap_fields.uid] = p

        for entry in udldap.list_people():
            person = people_by_uid.get(entry.uid, None)

            if person is None:
                fpr = entry.single("keyFingerPrint")
                if fpr:
                    log.warn("%s: %s has fingerprint %s and gid %s in LDAP, but is not in our db",
                             self.IDENTIFIER, entry.uid, fpr, entry.single("gidNumber"))
                else:
                    ldap_fields_args = {
                        "cn": entry.single("cn"),
                        "mn": entry.single("mn") or "",
                        "sn": entry.single("sn") or "",
                        "uid": entry.uid,
                        "email": entry.single("emailForward"),
                    }
                    person_args = {
                        "email": entry.single("emailForward"),
                        "username": "{}@invalid.example.org".format(entry.uid),
                        "fullname": _build_fullname(entry.single("cn"), entry.single("mn"), entry.single("sn")),
                    }
                    audit_args = {
                        "audit_author": self.hk.housekeeper.user,
                    }
                    if entry.is_dd:
                        person_args["status"] = const.STATUS_REMOVED_DD
                        audit_args["audit_notes"] = "created to mirror a removed DD account from LDAP"
                        if not person_args["email"]:
                            person_args["email"] = "{}@debian.org".format(entry.uid)
                    else:
                        person_args["status"] = const.STATUS_DC_GA
                        audit_args["audit_notes"] = "created to mirror a removed guest account from LDAP"
                        if not person_args["email"]:
                            person_args["email"] = "{}@example.org".format(entry.uid)
                    person = bmodels.Person.objects.create_user(**person_args, **audit_args)
                    dmodels.LDAPFields.objects.create(person=person, **ldap_fields_args, **audit_args)
                    log.warn("%s: %s: %s", self.IDENTIFIER, self.hk.link(person), audit_args["audit_notes"])
            else:
                dsa_status = entry.single("accountStatus")
                if dsa_status is not None:
                    dsa_status = dsa_status.split()[0]

                if dsa_status in ("retiring", "inactive"):
                    if person.status in (const.STATUS_DC_GA, const.STATUS_DM_GA):
                        pass  # TODO: handle guest accounts that have been closed
                    elif person.status not in (const.STATUS_REMOVED_DD, const.STATUS_EMERITUS_DD):
                        try:
                            process = pmodels.Process.objects.get(
                                    person=person, closed_by__isnull=True,
                                    applying_for__in=(const.STATUS_REMOVED_DD, const.STATUS_EMERITUS_DD))
                        except pmodels.Process.DoesNotExist:
                            process = None

                        if process is not None:
                            audit_notes = "closed from dsa: " + entry.single("accountStatus")
                            op = pops.ProcessClose(
                                audit_author=self.hk.housekeeper.user, audit_notes=audit_notes,
                                process=process,
                            )
                            op.execute()
                            person.refresh_from_db()
                            log.info("%s: %s %s", self.IDENTIFIER, self.hk.link(person), audit_notes)
                        else:
                            # mom040267 has a locked account (see the FD comments on the site)
                            if person.ldap_fields.uid != "mom040267":
                                log.warn("%s: %s has accountStatus '%s' (comment: %s) but in our db the state is %s",
                                         self.IDENTIFIER, self.hk.link(person), entry.single("accountStatus"),
                                         entry.single("accountComment"), const.ALL_STATUS_DESCS[person.status])

                if entry.is_dd and entry.single("keyFingerPrint") is not None:
                    if person.status not in (const.STATUS_DD_U, const.STATUS_DD_NU):
                        log.warn("%s: %s has supplementaryGid 'Debian' and fingerprint %s in LDAP, but in our db the state is %s",
                                 self.IDENTIFIER, self.hk.link(person), entry.single("keyFingerPrint"), const.ALL_STATUS_DESCS[person.status])

                email = entry.single("emailForward")
                if email != person.ldap_fields.email:
                    if email is not None:
                        log.info("%s: %s changing email_ldap from %s to %s (source: LDAP)",
                                 self.IDENTIFIER, self.hk.link(person), person.email, email)
                        person.ldap_fields.email = email
                        person.ldap_fields.save(audit_author=self.hk.housekeeper.user, audit_notes="updated ldap_fields.email from LDAP")
                    # It gives lots of errors when run outside of the debian.org
                    # network, since emailForward is not exported there, and it has
                    # no use case I can think of so far
                    #
                    # else:
                    #     log.info("%s: %s has email %s but emailForward is empty in LDAP",
                    #              self.IDENTIFIER, self.hk.link(person), person.email)

                # Update name information if it changes in LDAP
                for field in "cn", "mn", "sn":
                    old = getattr(person.ldap_fields, field)
                    new = entry.single(field)
                    if new is not None:
                        new = new.strip()
                    else:
                        new = ""
                    if old != new:
                        log.info("%s: %s changing %s from %s to %s (source: LDAP)",
                                 self.IDENTIFIER, self.hk.link(person), field, old, new)
                        setattr(person.ldap_fields, field, new)
                        person.ldap_fields.save(
                                audit_author=self.hk.housekeeper.user,
                                audit_notes="updated ldap_fields.{} from LDAP".format(field))
