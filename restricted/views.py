from __future__ import annotations
from django import http
from django.conf import settings
from django.shortcuts import redirect
from django.utils.translation import ugettext as _
from django.core.exceptions import PermissionDenied
from django.views.generic import View
from django.contrib import messages
import backend.models as bmodels
from backend.mixins import VisitorMixin, VisitorTemplateView
import json
import datetime
import os


class DBExport(VisitorMixin, View):
    require_visitor = "dd"

    def get(self, request, *args, **kw):
        from backend.export import export_db

        exported = export_db(full=False)

        class Serializer(json.JSONEncoder):
            def default(self, o):
                if hasattr(o, "strftime"):
                    return o.strftime("%Y-%m-%d %H:%M:%S")
                return json.JSONEncoder.default(self, o)

        res = http.HttpResponse(content_type="application/json")
        res["Content-Disposition"] = "attachment; filename=nm-mock.json"
        json.dump(exported, res, cls=Serializer, indent=1)
        return res


class Impersonate(View):
    def get(self, request, key=None, *args, **kw):
        visitor = request.user
        if not visitor.is_authenticated or not visitor.is_admin:
            raise PermissionDenied
        if key is None:
            del request.session["impersonate"]
            messages.add_message(request, messages.INFO, _("Impersonation canceled"))
        else:
            person = bmodels.Person.lookup_or_404(key)
            request.session["impersonate"] = person.lookup_key
            messages.info(request, _("Impersonating {}").format(person.lookup_key))

        url = request.GET.get("url", None)
        if url is None:
            return redirect('home')
        else:
            return redirect(url)


class MailboxStats(VisitorTemplateView):
    template_name = "restricted/mailbox-stats.html"
    require_visitor = "admin"

    def get_context_data(self, **kw):
        ctx = super(MailboxStats, self).get_context_data(**kw)

        try:
            with open(os.path.join(settings.DATA_DIR, 'mbox_stats.json'), "rt") as infd:
                stats = json.load(infd)
        except OSError:
            stats = {}

        for email, st in list(stats["emails"].items()):
            st["person"] = bmodels.Person.lookup_by_email(email)
            st["date_first_py"] = datetime.datetime.fromtimestamp(st["date_first"])
            st["date_last_py"] = datetime.datetime.fromtimestamp(st["date_last"])
            if "median" not in st or st["median"] is None:
                st["median_py"] = None
            else:
                st["median_py"] = datetime.timedelta(seconds=st["median"])
                st["median_hours"] = st["median_py"].seconds // 3600

        ctx.update(
            emails=sorted(stats["emails"].items()),
        )
        return ctx
