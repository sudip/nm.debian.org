from django.test import TestCase, Client
from django.urls import reverse
from keyring import models as kmodels
import json

test_fpr1 = "1793D6AB75663E6BF104953A634F4BD1E7AD5568"


class TestKeycheck(TestCase):
    def test_keycheck(self):
        kmodels.Key.objects.test_preload(test_fpr1)
        url = reverse("keyring_keycheck", args=[test_fpr1])
        client = Client()
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode("utf-8"))
        self.assertEqual(data["fpr"], test_fpr1)
        self.assertIsNone(data["person_id"])
        self.assertIsNone(data["person"])
        # self.assertEqual(data["errors"], [])
        self.assertEqual(len(data["uids"]), 4)
        self.assertRegex(data["uids"][0]["name"], r"Enrico Zini.+")

    def test_bad_fingerprint(self):
        url = reverse("keyring_keycheck", args=["AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"])
        client = Client()
        response = client.get(url)
        self.assertEqual(response.status_code, 400)
